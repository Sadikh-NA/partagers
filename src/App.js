import React from 'react';
import './App.css';

function App() {

  const handleClick = () => {
    navigator.share(
      {
        title: 'MDN',
        text: 'Learn web development on MDN!',
        url:'https://localhost',
      }
    )
    .then(() => console.log('Successful share'))
    .catch((error) => console.log('Error sharing', error));
  }

  return (
    <div className="App">
      <button onClick={handleClick}>Share </button>
    </div>
  );
}

export default App;
